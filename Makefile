PREFIX ?= /usr
MANDIR ?= $(PREFIX)/share/man

all:
	echo Run \'make install\' to install gnufetch.

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -p gnufetch $(DESTDIR)$(PREFIX)/bin/gnufetch
	cp -p gnufetch.1 $(DESTDIR)$(MANDIR)/man1

uninstall:
	rm -rf $(DESTDIR)$(PREFIX)/bin/gnufetch
	rm -rf $(DESTDIR)$(MANDIR)/man1/gnufetch.1*
