# gnufetch

![](screenshot.png)

## Usage

```
gnufetch - Prints short info about Debian.

Usage: gnufetch [OPTIONS]

OPTIONS:

    -a           No GNU art
    -c <color>   Set color for username (red, green, yellow, blue, magenta,
                 cyan, white)
    -l           No line
    -p           No package info
    -h           This help
```

## Install

Clone:

```
git clone http://codeberg.org/ghjardim/gnufetch.git
```

Enter clone directory and run make:

```
cd /path/to/gnufetch
sudo make install
```

## Uninstall

```
cd /path/to/gnufetch
sudo make uninstall
```

## Copyright

Copyright (C) 2021 Blau Araujo <blau@debxp.org>. \
Copyright (C) 2022 Guilherme H. Jardim <ghjardim@mail.shiori.com.br>. \
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.

GNU ASCII art: Copyright (C) 2001 Free Software Foundation, Inc. \
License GPLv2+: <https://www.gnu.org/licenses/old-licenses/gpl-2.0.html> \
or GFDLv1.1+: <https://www.gnu.org/licenses/fdl.html>.

gnufetch is derived from **debfetch**, written by Blau Araujo <blau@debxp.org>. \
debfetch is available on <https://git.blauaraujo.com/blau_araujo/debfetch>.
